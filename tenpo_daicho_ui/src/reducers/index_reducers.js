import { combineReducers } from 'redux'
import users from './user_reducers'
import { reducer as formReducer } from 'redux-form'

const rootReducer = combineReducers({
  user: users,
  form: formReducer
})
export default rootReducer;

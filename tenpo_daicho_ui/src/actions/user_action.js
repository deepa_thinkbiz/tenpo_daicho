import axios from 'axios';

export function userLogin(params){
  console.log("I m in action"+ JSON.stringify(params));
  const request = axios({
    method: 'post',
    url: 'http://localhost:3005/v1/users/sign_in',
    data: params,
    headers: []
  });
  return {
  type: "USER_LOGIN",
  payload: request
  }
}

export function userLoginSuccess(user){
  return{
      type: "USER_LOGIN_SUCCESS",
      payload: user
  }
}


export function userLoginFailure(err){
  return{
    type: "USER_LOGIN_FAILURE",
    payload: err
  }
}

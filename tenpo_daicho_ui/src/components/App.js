import React, { Component } from 'react';
import '../css/App.css';
import Routes from '../Routes/Routes';
import Header from '../layouts/Header';

class App extends Component {
  render() {
    return (
      <div className="App container">
        <Header/>
        <Routes/>
      </div>
    )
  }
}

export default App;

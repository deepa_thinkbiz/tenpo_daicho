import React ,{ Component } from 'react'
import { Field, reduxForm } from 'redux-form'
import { Link } from 'react-router-dom';

// const validate = (values) => {
//   const errors = {}
//
//   if (!values.email) {
//     errors.email = 'Email is required'
//   } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)) {
//     errors.email = 'Invalid email address'
//   }
//   if (!values.password) {
//     errors.password = 'Password is required'
//   }
//   return errors
// }
//
// const renderField = ({input,label,type,meta: { touched, error }}) =>
//   <div>
//     <div className="form-group">
//       <input {...input} placeholder={label} type={type} className="form-control"/>
//       {touched &&
//         ((error &&
//           <span style={{color: "red"}}>
//             {error}
//           </span>))}
//     </div>
//   </div>

class LoginForm extends Component{
  render(){
    const { handleSubmit,pristine, reset, submitting } = this.props
    return(
      <div>
        <div className="box">
          <form onSubmit={ handleSubmit }>
            <div className="field addon-set">
              <i className="fa fa-user"></i>
              <Field name="email" component="input" type="email" className= "padding-left-45"/>
            </div>
            <div className="field addon-set">
              <i className="fa fa-key"></i>
              <Field name="password" component="input" type="password" className= "padding-left-45"/>
            </div>
            <div>
              <button type="submit" disabled={submitting} className="btn btn-primary">Login</button>
            </div>
          </form>
        </div>
      </div>
    )
  }
}
LoginForm = reduxForm({
  // a unique name for the form
  form: 'LoginForm'
  // validate
})(LoginForm)

export default LoginForm;

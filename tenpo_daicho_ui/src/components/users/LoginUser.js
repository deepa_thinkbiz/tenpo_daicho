import React ,{ Component } from 'react'
import LoginForm from './LoginForm'
import { withRouter } from 'react-router-dom'

class LoginUser extends Component{
  submit = (values) => {
    console.log("hii"+ values);
    this.props.userLogin(values);
  }

  componentWillReceiveProps(nextProps) {
    console.log(nextProps.loginUser.success);
    if(nextProps.loginUser.success){
      this.props.history.push('/');
    }
  }

  render(){
    const { user, loading, error,success} = this.props.loginUser;
    console.log("hello I entered");
      return(
        <div>
          <h1>User Login</h1>
          <LoginForm onSubmit={this.submit}/>
        </div>
      )
  }
}

export default withRouter(LoginUser);
 // export default LoginUser;

import React from 'react';
import { Route, Switch } from 'react-router-dom';
// import UserLoginPage from '../components/users/LoginUser'
import UserLoginPage from '../pages/user/UserLoginPage';
import Home from '../containers/Home';

export default ( ) => (
  <Switch>
    <Route path= "/" exact component={Home} />
    <Route path = "/login" exact component = {UserLoginPage} />
  </Switch>
);

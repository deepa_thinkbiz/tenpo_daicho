Rails.application.routes.draw do
  scope module: 'api' do
    namespace :v1 do
      devise_for :users
    end
  end
  # devise_for :users
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root 'users#index'
end
